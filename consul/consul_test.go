package consul

import (
	"encoding/json"
	"testing"
)

var consulCli *Client

func init() {
	consulCli, _ = NewConsulClient("http://127.0.0.1:8500")
}

func TestInit(t *testing.T) {
	err := consulCli.Register("test", "007", "", 6379, "hubilie")
	if err != nil {
		t.Fatal(err)
	}
	svr, err := consulCli.Services("test", "hubilie", false)
	if err != nil {
		t.Fatal(err)
	}
	ab, err := json.Marshal(svr)
	t.Log(string(ab))
	err = consulCli.UnRegister("test", "007")
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetKeyValue(t *testing.T) {
	err := consulCli.StoreKeyValue("test", []byte(`{"data":"xiexie"}`))
	if err != nil {
		t.Fatal(err)
	}
	ab, err := consulCli.GetKeyValue("test")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(ab))
	err = consulCli.DelKeyValue("test")
	if err != nil {
		t.Fatal(err)
	}
}
