package consul

import (
	"fmt"
	stdConsul "github.com/hashicorp/consul/api"
	"net/url"
	"os"
)

type SvrNode struct {
	ID      string
	Service string
	Address string
	Port    int
	Tags    []string
}

type Client struct {
	cli *stdConsul.Client
}

// NewConsulClient 建立连接
func NewConsulClient(addr string) (*Client, error) {
	urlObj, err := url.Parse(addr)
	if err != nil {
		return nil, fmt.Errorf("urlparse failed. url(%s) %v", addr, err)
	}
	consulConfig := stdConsul.DefaultConfig()
	var cAddr string
	if urlObj.Port() == "" {
		cAddr = urlObj.Hostname()
	} else {
		cAddr = fmt.Sprintf("%s:%s", urlObj.Hostname(), urlObj.Port())
	}
	consulConfig.Address = cAddr
	consulConfig.Scheme = urlObj.Scheme

	client, err := stdConsul.NewClient(consulConfig)
	if err != nil {
		return nil, fmt.Errorf("consul: consul.NewClient failed %v", err)
	}
	return &Client{
		cli: client,
	}, nil
}

// Register 注册服务
// id为空会使用当前服务host
func (c *Client) Register(appName string, id string, host string, port int, tags ...string) error {
	var err error
	if host == "" {
		host, err = os.Hostname()
		if err != nil {
			return fmt.Errorf("consul: os.Hostname failed %v", err)
		}
	}
	if id == "" {
		id = host
	}
	var tag = []string{"metrics", id}
	for _, v := range tags {
		tag = append(tag, v)
	}
	var registration = &stdConsul.AgentServiceRegistration{
		ID:      fmt.Sprintf("%s.%s", appName, id), //当前实例ID 容器启动可以直接用容器host
		Name:    appName,                           //服务名称同一个服务用同一个
		Tags:    tag,
		Port:    port, //端口
		Address: host, //地址
		Check: &stdConsul.AgentServiceCheck{ //健康检查
			TCP:                            fmt.Sprintf("%s:%d", host, port), //健康检查地址
			Timeout:                        "3s",                             //超时时间
			Interval:                       "5s",                             //多久检查一次
			DeregisterCriticalServiceAfter: "15s",                            //15秒没有收到返回数据就在发现服务摘除服务
		},
	}
	err = c.cli.Agent().ServiceRegister(registration)
	if err != nil {
		return fmt.Errorf("consul: client.Agent().ServiceRegister failed. %v", err)
	}
	return nil
}

// UnRegister 注销
func (c *Client) UnRegister(appName string, id string) error {
	var err error
	if id == "" {
		id, err = os.Hostname()
		if err != nil {
			return fmt.Errorf("consul: os.Hostname failed %v", err)
		}
	}
	err = c.cli.Agent().ServiceDeregister(fmt.Sprintf("%s.%s", appName, id))
	if err != nil {
		return fmt.Errorf("consul: client.Agent().ServiceDeregister id %s.%s failed %v", appName, id, err)
	}
	return nil
}

// Services 获取服务列表
func (c *Client) Services(appName string, tag string, healthyOnly bool) ([]*SvrNode, error) {
	svr, _, err := c.cli.Health().Service(appName, tag, healthyOnly, nil)
	if err != nil {
		return nil, err
	}
	var svrArr []*SvrNode
	for _, v := range svr {
		svrArr = append(svrArr, &SvrNode{
			ID:      v.Service.ID,
			Service: v.Service.Service,
			Address: v.Service.Address,
			Port:    v.Service.Port,
			Tags:    v.Service.Tags,
		})
	}
	return svrArr, nil
}

func (c *Client) StoreKeyValue(key string, value []byte) error {
	kv := &stdConsul.KVPair{
		Key:   key,
		Flags: 0,
		Value: value,
	}
	_, err := c.cli.KV().Put(kv, nil)
	if err != nil {
		return fmt.Errorf("consul: client.KV().Put failed %v", err)
	}
	return nil
}

func (c *Client) GetKeyValue(key string) ([]byte, error) {
	kv, _, err := c.cli.KV().Get(key, nil)
	if err != nil {
		return nil, fmt.Errorf("consul: client.KV().Get failed %v", err)
	}
	return kv.Value, nil
}

func (c *Client) DelKeyValue(key string) error {
	_, err := c.cli.KV().Delete(key, nil)
	if err != nil {
		return fmt.Errorf("consul: client.KV().Delete failed %v", err)
	}
	return nil
}
