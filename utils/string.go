package utils

import (
	"bytes"
	"fmt"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
	"io/ioutil"
)

func NumberFormat(n int64) string {
	var ret string
	if n%10000 != 0 {
		ret = fmt.Sprintf("%d", n%10000)
	}
	n = n / 10000
	if n > 0 {
		if n%10000 != 0 {
			ret = fmt.Sprintf("%d万%s", n%10000, ret)
		}
		if n >= 10000 {
			n = n / 10000
			ret = fmt.Sprintf("%d亿%s", n, ret)
		}
	}
	if ret == "" {
		return "0"
	}
	return ret
}

func StringToBytes(src string, des []byte) {
	ret, _ := Utf8ToGbk([]byte(src))
	var i int
	for i = 0; i < len(ret); i++ {
		if i >= len(des) {
			break
		}
		des[i] = ret[i]
	}
}

func GbkToUtf8(s []byte) ([]byte, error) {
	reader := transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewDecoder())
	d, e := ioutil.ReadAll(reader)
	if e != nil {
		return nil, e
	}
	return d, nil
}

func Utf8ToGbk(s []byte) ([]byte, error) {
	reader := transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewEncoder())
	d, e := ioutil.ReadAll(reader)
	if e != nil {
		return nil, e
	}
	return d, nil
}
