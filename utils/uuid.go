package utils

import (
	"github.com/hashicorp/go-uuid"
	"strings"
)

func UUID() (string, error) {
	s, err := uuid.GenerateUUID()
	if err != nil {
		return "", err
	}
	return strings.Replace(s, "-", "", -1), nil
}
