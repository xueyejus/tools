package utils

import (
	"fmt"
	"math/rand"
)

func GenerateRandomString(length uint8) (string, error) {
	const chars = "abcdefghijklmnopqrstuvwxyz0123456789"
	l := len(chars)
	result := make([]byte, length)
	_, err := rand.Read(result)
	if err != nil {
		return "", fmt.Errorf("Error reading random bytes: %v", err)
	}
	for i := 0; i < int(length); i++ {
		result[i] = chars[int(result[i])%l]
	}
	return string(result), nil
}
