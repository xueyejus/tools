package utils

import (
	"errors"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func doPost(client *http.Client, url, contentType, content string) ([]byte, error) {
	resp, err := client.Post(url,
		contentType,
		strings.NewReader(content))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	rb, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return rb, nil
}

func DoPost(url, contentType, content string, timeout time.Duration) ([]byte, error) {
	client := &http.Client{
		Timeout: timeout,
	}
	if url == "" {
		return nil, errors.New("post: request url is empty")
	}
	if contentType == "" {
		contentType = "application/json;charset=utf-8"
	}
	return doPost(client, url, contentType, content)
}
