package utils

import (
	"reflect"
	"unicode"
	"unicode/utf8"
)

func IsExported(t reflect.Type) bool {
	w, _ := utf8.DecodeRuneInString(t.Name())
	return unicode.IsUpper(w)
}
