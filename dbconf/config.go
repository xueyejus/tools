package dbconf

import (
	"fmt"
	"sync"
)

type tableUpdateListener struct {
	UpdateTimestamp int64
	CallBack        ConfigUpdateListener
}
type ConfigUpdateListener func() error

var updateListenerMap = make(map[string]*tableUpdateListener)
var updateListenerLocker sync.RWMutex

func AddConfigUpdateListener(tableName string, f ConfigUpdateListener) {
	updateListenerLocker.Lock()
	defer updateListenerLocker.Unlock()
	updateListenerMap[tableName] = &tableUpdateListener{
		UpdateTimestamp: -1,
		CallBack:        f,
	}
}

func CheckConfigUpdate(m map[string]int64) error {
	updateListenerLocker.RLock()
	defer updateListenerLocker.RUnlock()
	var updateTime int64
	for t, l := range updateListenerMap {
		updateTime = 0
		if v, ok := m[t]; ok {
			updateTime = v
		}
		if updateTime != l.UpdateTimestamp {
			err := l.CallBack()
			if err != nil {
				return fmt.Errorf("table:%s update failed. err:%v", t, err)
			}
			l.UpdateTimestamp = updateTime
		}
	}
	return nil
}
