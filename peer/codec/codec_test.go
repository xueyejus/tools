package codec

import (
	"errors"
	"gitlab.stars.game/ogbg/tools/peer/codec/baseproto"
	"log"
	"testing"
)

type TestMsg struct {
	Id      int
	Content string
}

func TestWsJsonCodec_Unmarshal(t *testing.T) {
	c := GetCodec("protobuf_codec")
	RegisterMessage("1", (*baseproto.PingPang)(nil))
	//req := baseproto.PingPang{
	//	Timestamp: 123,
	//}
	rdata, err := c.Marshal("1", &baseproto.PingPang{
		Timestamp: 123,
	}, nil)
	if err != nil {
		t.Fatal(err)
	}
	_, pack, err := c.Unmarshal(rdata)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(c.ToString(pack.DataPtr), pack.Err)
}

func TestGobCodec(t *testing.T) {
	c := GetCodec("gob_codec")
	RegisterMessage("2", (*TestMsg)(nil))
	//req := TestMsg{
	//	Id:      123,
	//	Content: "测试测试",
	//}
	rdata, err := c.Marshal("2", nil, errors.New("测试错误"))
	if err != nil {
		t.Fatal(err)
	}
	_, pack, err := c.Unmarshal(rdata)
	if err != nil {
		t.Fatal(err)
	}
	log.Println(c.ToString(pack.DataPtr), pack.Err)
}
