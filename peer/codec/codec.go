package codec

import (
	"fmt"
	"sync"
)

type Codec interface {
	Marshal(router string, dataPtr interface{}, err error) ([]byte, error)
	Unmarshal([]byte) (int, *MsgPack, error)
	ToString(interface{}) string
}

var codecsList = make(map[string]Codec)
var codecLock sync.RWMutex

func RegisterCodec(name string, codec Codec) error {
	codecLock.Lock()
	defer codecLock.Unlock()
	if codec == nil {
		return fmt.Errorf("codec: Register provide is nil")
	}
	if _, dup := codecsList[name]; dup {
		return fmt.Errorf("codec: name:%s registed", name)
	}
	codecsList[name] = codec
	return nil
}

func GetCodec(name string) Codec {
	codecLock.RLock()
	defer codecLock.RUnlock()
	if v, ok := codecsList[name]; ok {
		return v
	}
	return nil
}
