package conn

import (
	"errors"
	"github.com/gorilla/websocket"
	"net"
	"net/http"
	"sync"
	"time"
)

const (
	writeWait          = 20 * time.Second
	pongWait           = 60 * time.Second
	pingPeriod         = (pongWait * 9) / 10
	maxFrameMessageLen = 16 * 1024 //4 * 4096
	maxSendBuffer      = 16
)

var (
	ErrBrokenPipe       = errors.New("send to broken pipe")
	ErrBufferPoolExceed = errors.New("send buffer exceed")
	ErrSendBufferLimit  = errors.New("send buffer limit")
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
	ReadBufferSize:  maxFrameMessageLen,
	WriteBufferSize: maxFrameMessageLen,
}

type wsClient struct {
	hub        *Hub
	conn       *websocket.Conn
	send       chan []byte
	running    bool
	closeMutex sync.RWMutex
	clientIP   string
}

// Raw 取原始连接
func (c *wsClient) Raw() interface{} {
	if c.conn == nil {
		return nil
	}
	return c.conn
}

// RemoteAddr 访问地址
func (c *wsClient) RemoteAddr() string {
	if c.conn == nil {
		return ""
	}
	return c.conn.RemoteAddr().String()
}

// ClientIP
func (c *wsClient) ClientIP() string {
	return c.clientIP
}

// Close 关闭连接
func (c *wsClient) Close() {
	if !c.running {
		return
	}
	c.closeMutex.Lock()
	defer c.closeMutex.Unlock()
	if c.running {
		c.running = false
		c.conn.Close()
	}
}

// Send 发送消息
func (c *wsClient) Send(msg []byte) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = ErrBrokenPipe
		}
	}()
	if !c.running {
		return ErrBrokenPipe
	}
	if len(c.send) >= maxSendBuffer {
		return ErrBufferPoolExceed
	}
	if len(msg) > maxFrameMessageLen {
		return ErrSendBufferLimit
	}
	c.send <- msg
	return nil
}

// 接收循环
func (c *wsClient) recvLoop() {
	defer func() {
		c.running = false
		c.hub.unregister <- c
		c.conn.Close()
		close(c.send)
	}()
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error {
		c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	for c.conn != nil {
		_, data, err := c.conn.ReadMessage()
		if err != nil {
			return
		}
		c.hub.processMessage(c, data)
	}
}

func (c *wsClient) sendLoop() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		c.running = false
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case msg, ok := <-c.send:
			if !ok {
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.BinaryMessage, msg); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

func (c *wsClient) IsClosed() bool {
	c.closeMutex.RLock()
	defer c.closeMutex.RUnlock()
	return !c.running
}

func ServeWs(hub *Hub, w http.ResponseWriter, r *http.Request) error {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return err
	}
	c := &wsClient{
		hub:      hub,
		conn:     conn,
		send:     make(chan []byte, maxSendBuffer),
		clientIP: getClientIP(r)}
	c.hub.register <- c
	go c.recvLoop()
	go c.sendLoop()
	c.running = true
	return nil
}

func getClientIP(r *http.Request) string {
	ip := r.Header.Get("X-Forwarded-For")
	if ip == "" {
		ip = r.RemoteAddr
	}
	host, _, err := net.SplitHostPort(ip)
	if err != nil {
		return ip
	}
	var parseIP = net.ParseIP(host)
	if parseIP != nil {
		return parseIP.String()
	}
	return ""
}
