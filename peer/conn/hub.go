package conn

type Client interface {
	Raw() interface{}
	RemoteAddr() string
	Close()
	Send(msg []byte) (err error)
	IsClosed() bool
	ClientIP() string
}

type ClientCallBack interface {
	OnConnected(Client)
	OnClosed(Client)
	OnReceive(Client, []byte) error
}

// Hub 客户端管理
type Hub struct {
	// 客户端列表
	clients map[Client]bool

	// Register requests from the clients.
	register chan Client

	// Unregister requests from clients.
	unregister chan Client

	// CallBack 与业务沟通回调
	callback ClientCallBack
}

func NewHub(callback ClientCallBack) *Hub {
	h := &Hub{
		register:   make(chan Client),
		unregister: make(chan Client),
		clients:    make(map[Client]bool),
		callback:   callback,
	}
	go h.run()
	return h
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
			go h.callback.OnConnected(client)
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
			}
			go h.callback.OnClosed(client)
		}
	}
}

func (h *Hub) processMessage(c Client, msg []byte) {
	if h.callback != nil {
		go func() {
			err := h.callback.OnReceive(c, msg)
			if err != nil {
				// 错误消息关闭连接
				c.Close()
			}
		}()
	}
}
