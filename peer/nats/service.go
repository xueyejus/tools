package nats

import (
	"fmt"
	"gitlab.stars.game/ogbg/tools/utils"
	"reflect"
	"strings"
)

var (
	typeOfError = reflect.TypeOf((*error)(nil)).Elem()
)

type natsService struct {
	name     string // 服务名
	rValue   reflect.Value
	rType    reflect.Type
	handlers map[string]reflect.Method // 注册的方法列表
}

func newNatsService(name string, comp interface{}) (*natsService, error) {
	s := &natsService{
		rValue: reflect.ValueOf(comp),
		rType:  reflect.Indirect(reflect.ValueOf(comp)).Type(),
	}
	if name == "" {
		if !utils.IsExported(s.rType) {
			return nil, fmt.Errorf("type:%s is not exported", s.rType.Name())
		}
		s.name = strings.ToLower(s.rType.Name())
	} else {
		s.name = name
	}
	return s, nil
}

// 遍历取出满足条件的函
