package nats

import (
	"fmt"
	"gitlab.stars.game/ogbg/tools/utils"
	"reflect"
)

// 方法检测
func isHandlerMethod(method reflect.Method) bool {
	mt := method.Type
	if mt.NumIn() != 2 {
		return false
	}
	if mt.In(1).Kind() != reflect.Ptr {
		return false
	}
	if mt.NumOut() == 1 {
		if mt.Out(0) != typeOfError {
			return false
		}
	} else if mt.NumOut() == 2 {
		if mt.Out(1) != typeOfError || mt.Out(0).Kind() != reflect.Ptr {
			return false
		}
	} else {
		return false
	}
	return true
}

func callHandlerFunc(foo reflect.Method, args []reflect.Value) (retValue interface{}, retErr error) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(fmt.Sprintf("callHandlerFunc: %v", err))
			fmt.Println(utils.CallStack())
			retValue = nil
			retErr = fmt.Errorf("callHandlerFunc: call method pkg:%s method:%s err:%v", foo.PkgPath, foo.Name, err)
		}
	}()
	ret := foo.Func.Call(args)
	var err error = nil
	if len(ret) == 1 {
		if r1 := ret[0].Interface(); r1 != nil {
			err = r1.(error)
		}
		return nil, err
	} else if len(ret) == 2 {
		if r1 := ret[1].Interface(); r1 != nil {
			err = r1.(error)
		}
		return ret[0].Interface(), err
	}
	return nil, fmt.Errorf("callHandlerFunc: call method pkg:%s method:%s retn:%d", foo.PkgPath, foo.Name, len(ret))
}
