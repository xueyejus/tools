package nats

import (
	"fmt"
	"github.com/nats-io/nats.go"
	"gitlab.stars.game/ogbg/tools/peer"
	"gitlab.stars.game/ogbg/tools/peer/codec"
	"reflect"
	"strings"
	"time"
)

type NatsCli struct {
	conn       *nats.Conn
	c          codec.Codec
	serverList map[string]*natsService
}

func NewNatsCli(addr, codecType string) (*NatsCli, error) {
	conn, err := nats.Connect(addr, nats.Timeout(3*time.Second))
	if err != nil {
		return nil, fmt.Errorf("nats connect failed. err:%v", err)
	}
	cli := &NatsCli{
		conn:       conn,
		serverList: make(map[string]*natsService),
	}
	cliCodec := codec.GetCodec(codecType)
	if cliCodec == nil {
		return nil, fmt.Errorf("codec:%s not found. ", codecType)
	}
	cli.c = cliCodec
	return cli, nil
}

func (m *NatsCli) Call(subj, api string, req, ack interface{}) error {
	if reflect.TypeOf(req).Kind() != reflect.Ptr {
		return fmt.Errorf("nats: rpc call parameter cannot be a non-pointer req:%T", req)
	}
	if ack != nil {
		if reflect.TypeOf(ack).Kind() != reflect.Ptr {
			return fmt.Errorf("nats: rpc call parameter cannot be a non-pointer ack:%T", ack)
		}
		if codec.GetMessage(api) == nil {
			if err := codec.RegisterMessage(api, ack); err != nil {
				return fmt.Errorf("server: router %s param %s registed", api, reflect.TypeOf(ack))
			}
		}
	}
	rb, err := m.c.Marshal(api, req, nil)
	if err != nil {
		return err
	}
	msg, err := m.conn.Request(subj, rb, time.Second*5)
	if err != nil {
		return err
	}
	if ack != nil {
		_, pack, err := m.c.Unmarshal(msg.Data)
		if err != nil {
			return err
		}
		if pack.Err != nil {
			return pack.Err
		}
		if pack.DataPtr != nil {
			reflect.ValueOf(ack).Elem().Set(reflect.ValueOf(pack.DataPtr).Elem())
		}
	}
	return nil
}

func (m *NatsCli) natsMessageHandler(msg *nats.Msg) {
	_, msgPack, err := m.c.Unmarshal(msg.Data)
	if err != nil {
		fmt.Printf("onreceive: %v", err)
		return
	}
	router, ok := msgPack.Router.(string)
	if !ok {
		fmt.Printf("onreceive: invalid router:%v", msgPack.Router)
		return
	}
	routerArr := strings.Split(router, ".")
	if len(routerArr) != 2 {
		fmt.Printf("onreceive: invalid router:%s", msgPack.Router)
		return
	}
	s, ok := m.serverList[routerArr[0]]
	if !ok {
		fmt.Printf("onreceive: function not registed router:%s err:%v", msgPack.Router, err)
		return
	}
	h, ok := s.handlers[routerArr[1]]
	if !ok {
		fmt.Printf("onreceive: function not registed router:%s err:%v", msgPack.Router, err)
		return
	}
	var args = []reflect.Value{s.rValue, reflect.ValueOf(msgPack.DataPtr)}
	ack, err := callHandlerFunc(h, args)
	if err != nil {
		fmt.Println(fmt.Sprintf("onreceive: call func failed. router:%s err:%v", msgPack.Router, err))
		rb, err := m.c.Marshal(router, nil, err)
		if err != nil {
			fmt.Println(err)
			return
		}
		err = msg.Respond(rb)
		if err != nil {
			fmt.Println(err)
			return
		}
	} else {
		if ack != nil && !reflect.ValueOf(ack).IsNil() {
			rb, err := m.c.Marshal(router, ack, nil)
			if err != nil {
				fmt.Println(err)
				return
			}
			err = msg.Respond(rb)
			if err != nil {
				fmt.Println(err)
				return
			}
		}
	}
}

func (m *NatsCli) registerHandler(name string, svr interface{}) error {
	s, err := newNatsService(name, svr)
	if err != nil {
		return fmt.Errorf("register component failed. comp:%T err:%v", svr, err)
	}
	if _, ok := m.serverList[s.name]; ok {
		return fmt.Errorf("server already defined: %s", s.name)
	}
	s.handlers, err = peer.GetValidMethods(reflect.TypeOf(svr), isHandlerMethod)
	if err != nil {
		return err
	}
	for name, method := range s.handlers {
		router := fmt.Sprintf("%s.%s", s.name, name)
		if err := codec.RegisterMessage(router, method.Type.In(1)); err != nil {
			return fmt.Errorf("server: router %s param %s registed", router, method.Type.In(1).Name())
		}
		//if method.Type.NumOut() == 2 {
		//	//注册消息 用于解码
		//	if err := codec.RegisterMessage(router, method.Type.Out(0)); err != nil {
		//		return fmt.Errorf("server: router %s param %s registed", router, method.Type.Out(0).Name())
		//	}
		//}
	}
	m.serverList[s.name] = s
	return nil
}

func (m *NatsCli) RPCHandler(subj, queue string, svrs ...interface{}) error {
	if subj == "" {
		return fmt.Errorf("invalid subj:%s", subj)
	}
	for _, v := range svrs {
		err := m.registerHandler("", v)
		if err != nil {
			return fmt.Errorf("service: register failed. subj:%s queue:%s svr:%T err:%v", subj, queue, v, err)
		}
	}
	_, err := m.conn.QueueSubscribe(subj, queue, m.natsMessageHandler)
	if err != nil {
		return err
	}
	return nil
}

func (m *NatsCli) SubscribeHandler(subj string, svrs ...interface{}) error {
	if subj == "" {
		return fmt.Errorf("invalid subj:%s", subj)
	}
	for _, v := range svrs {
		err := m.registerHandler("", v)
		if err != nil {
			return fmt.Errorf("service: register failed. subj:%s svr:%T err:%v", subj, v, err)
		}
	}
	_, err := m.conn.Subscribe(subj, m.natsMessageHandler)
	if err != nil {
		return err
	}
	return nil
}

// Publish 消息广播
func (m *NatsCli) Publish(subj, api string, req interface{}) error {
	rb, err := m.c.Marshal(api, req, nil)
	if err != nil {
		return err
	}
	err = m.conn.Publish(subj, rb)
	if err != nil {
		return err
	}
	err = m.conn.Flush()
	return err
}
