package peer

import (
	"fmt"
	"reflect"
	"strings"
)

type CheckHandleMethodFunc func(reflect.Method) bool

func GetValidMethods(t reflect.Type, handleFunc CheckHandleMethodFunc) (map[string]reflect.Method, error) {
	methods := make(map[string]reflect.Method)
	for m := 0; m < t.NumMethod(); m++ {
		method := t.Method(m)
		if handleFunc(method) {
			methods[strings.ToLower(method.Name)] = method
		}
	}
	if len(methods) == 0 {
		return nil, fmt.Errorf("type:%s has no exported methods", t.Name())
	}
	return methods, nil
}
