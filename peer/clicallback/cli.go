package clicallback

import (
	"fmt"
	"gitlab.stars.game/ogbg/tools/peer"
	"gitlab.stars.game/ogbg/tools/peer/codec"
	"gitlab.stars.game/ogbg/tools/peer/conn"
	"gitlab.stars.game/ogbg/tools/utils"
	"reflect"
	"strings"
	"sync"
	"time"
)

var (
	typeOfError  = reflect.TypeOf((*error)(nil)).Elem()
	typeOfClient = reflect.TypeOf((*Session)(nil))
)

type cliService struct {
	name      string                    // 服务名
	handlers  map[string]reflect.Method // 注册的方法列表
	rType     reflect.Type
	component Component
	bActive   bool
}

func newCliService(name string, comp Component, bActive bool) (*cliService, error) {
	s := &cliService{
		component: comp,
		rType:     reflect.Indirect(reflect.ValueOf(comp)).Type(),
		bActive:   bActive,
	}
	if name == "" {
		if !utils.IsExported(s.rType) {
			return nil, fmt.Errorf("type:%s is not exported", s.rType.Name())
		}
		s.name = strings.ToLower(s.rType.Name())
	} else {
		s.name = name
	}
	return s, nil
}

type CliServer struct {
	clientList      map[conn.Client]*Session
	clientListMutex sync.RWMutex
	serverList      map[string]*cliService
	routerCodec     codec.Codec
}

func (m *CliServer) Register(bActive bool, comp ...Component) error {
	for _, v := range comp {
		if err := m.RegisterOne("", v, bActive); err != nil {
			return err
		}
	}
	return nil
}

func (m *CliServer) RegisterOne(name string, comp Component, bActive bool) error {
	s, err := newCliService(name, comp, bActive)
	if err != nil {
		return fmt.Errorf("register component failed. name:%s comp:%T err:%v", name, comp, err)
	}
	if _, ok := m.serverList[s.name]; ok {
		return fmt.Errorf("server already defined: %s", s.name)
	}
	s.handlers, err = peer.GetValidMethods(reflect.TypeOf(s.component), isHandlerMethod)
	if err != nil {
		return err
	}
	for name, method := range s.handlers {
		router := fmt.Sprintf("%s.%s", s.name, name)
		//注册消息 用于解码
		if err := codec.RegisterMessage(router, method.Type.In(2)); err != nil {
			return fmt.Errorf("server: router %s param %s registed", router, method.Type.In(2).Name())
		}
	}
	m.serverList[s.name] = s
	return nil
}

func (m *CliServer) run() {
	t := time.NewTicker(time.Second * 30)
	defer t.Stop()
	for {
		<-t.C
		for _, v := range m.clientList {
			if checkClientClosed(v) == true {
				v.Close()
			}
		}
	}
}

func NewCliServer(codecName string) (*CliServer, error) {
	routerCodec := codec.GetCodec(codecName)
	if routerCodec == nil {
		return nil, fmt.Errorf("server: codec %s not registered", codecName)
	}
	cliSvr := &CliServer{
		serverList:  make(map[string]*cliService),
		routerCodec: routerCodec,
		clientList:  make(map[conn.Client]*Session),
	}
	go cliSvr.run()
	return cliSvr, nil
}

func checkClientClosed(client *Session) bool {
	activeTime := client.activeTime
	if p := client.GetBindPlayer(); p != nil {
		// 已登录用户
		if activeTime <= time.Now().Add(time.Minute*-30).Unix() {
			return true
		}
	} else {
		// 未登录用户
		if activeTime <= time.Now().Add(time.Minute*-5).Unix() {
			return true
		}
	}
	return false
}

//func NewHub(codecName string) (*conn.Hub, error) {
//	routerCodec := codec.GetCodec(codecName)
//	if routerCodec == nil {
//		return nil, fmt.Errorf("server: codec %s not registered", codecName)
//	}
//	cs := &CliServer{
//		serverList:  make(map[string]*cliService),
//		routerCodec: routerCodec,
//	}
//	go cs.run()
//	return conn.NewHub(cs), nil
//}
