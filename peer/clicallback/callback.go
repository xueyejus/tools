package clicallback

import (
	"fmt"
	"gitlab.stars.game/ogbg/tools/peer/conn"
	"gitlab.stars.game/ogbg/tools/utils"
	"reflect"
	"strings"
	"time"
)

func (m *CliServer) getSession(client conn.Client) *Session {
	m.clientListMutex.RLock()
	defer m.clientListMutex.RUnlock()
	return m.clientList[client]
}

// OnConnected 新得链接
func (m *CliServer) OnConnected(client conn.Client) {
	m.clientListMutex.Lock()
	defer m.clientListMutex.Unlock()
	if _, ok := m.clientList[client]; !ok {
		m.clientList[client] = &Session{
			client:      client,
			routerCodec: m.routerCodec,
			activeTime:  time.Now().Unix(),
		}
	}
}

func (m *CliServer) OnClosed(client conn.Client) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(fmt.Sprintf("OnClosed: err:%v", err))
			fmt.Println(utils.CallStack())
		}
	}()
	sess := m.getSession(client)
	if sess == nil {
		return
	}
	for _, v := range m.serverList {
		if ok := v.component.OnSessionClose(sess); ok {
			return
		}
	}
}

// OnReceive 接收到消息后处理
func (m *CliServer) OnReceive(client conn.Client, msg []byte) error {
	sess := m.getSession(client)
	if sess == nil {
		return nil
	}
	_, msgPack, err := m.routerCodec.Unmarshal(msg)
	if err != nil {
		return fmt.Errorf("onreceive: %v", err)
	}
	router, ok := msgPack.Router.(string)
	if !ok {
		return fmt.Errorf("onreceive: invalid router:%v", msgPack.Router)
	}
	routerArr := strings.Split(router, ".")
	if len(routerArr) != 2 {
		return fmt.Errorf("onreceive: invalid router:%s", msgPack.Router)
	}
	s, ok := m.serverList[routerArr[0]]
	if !ok {
		return fmt.Errorf("onreceive: function not registed router:%s err:%v", msgPack.Router, err)
	}
	h, ok := s.handlers[routerArr[1]]
	if !ok {
		return fmt.Errorf("onreceive: function not registed router:%s err:%v", msgPack.Router, err)
	}
	t1 := time.Now()
	var args = []reflect.Value{reflect.ValueOf(s.component), reflect.ValueOf(sess), reflect.ValueOf(msgPack.DataPtr)}
	ack, err := callHandlerFunc(h, args)
	if ack != nil && !reflect.ValueOf(ack).IsNil() {
		rb, err := m.routerCodec.Marshal(router, ack, nil)
		if err != nil {
			return fmt.Errorf("service: m.routerCodec.Marshalfailed. %v", err)
		}
		_ = client.Send(rb)
	} else {
		if h.Type.NumOut() == 2 {
			rb, err := m.routerCodec.Marshal(router, nil, err)
			if err != nil {
				return fmt.Errorf("service: m.routerCodec.Marshalfailed. %v", err)
			}
			_ = client.Send(rb)
		}
	}
	var errMsg string
	if err != nil {
		errMsg = err.Error()
	}
	dt := time.Since(t1)
	go s.component.OnRequestFinished(sess, router, m.routerCodec.ToString(msgPack.DataPtr), errMsg, dt)
	if s.bActive {
		sess.activeTime = time.Now().Unix()
	}
	return nil
}
