package clicallback

import (
	"time"
)

type Component interface {
	OnSessionClose(*Session) bool
	OnRequestFinished(*Session, string, interface{}, string, time.Duration)
}

type ComponentBase struct{}

func (c *ComponentBase) OnSessionClose(_ *Session) bool { return false }

func (c *ComponentBase) OnRequestFinished(_ *Session, _ string, _ interface{}, _ string, _ time.Duration) {
}
