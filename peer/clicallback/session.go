package clicallback

import (
	"fmt"
	"gitlab.stars.game/ogbg/tools/peer/codec"
	"gitlab.stars.game/ogbg/tools/peer/conn"
)

type Session struct {
	client      conn.Client
	routerCodec codec.Codec
	kvExt
	activeTime int64
	p          interface{}
}

func (s *Session) Send(router string, data interface{}) error {
	rb, err := s.routerCodec.Marshal(router, data, nil)
	if err != nil {
		return fmt.Errorf("service: %v", err)
	}
	return s.client.Send(rb)
}

func (s *Session) Bind(player interface{}) {
	s.p = player
}

func (s *Session) GetBindPlayer() interface{} {
	return s.p
}

func (s *Session) Close() {
	s.client.Close()
}

func (s *Session) RemoteAddr() string {
	return s.client.RemoteAddr()
}

func (s *Session) ClientIP() string {
	return s.client.ClientIP()
}
