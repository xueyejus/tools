package clicallback

import (
	"testing"
)

var funcRegisterTest = []struct {
	h     string
	exist bool
}{
	{"testhandler", true},
	{"testfunc", true},
	{"testfunc1", true},
	{"testfunc2", false},
	{"testfunc3", false},
	{"testfunc4", false},
}

type TestMessage struct {
	Id      int    `json:"id"`
	Content string `json:"content"`
}
type TestService struct {
	ComponentBase
}

func (s *TestService) TestHandler(_ *Session, message *TestMessage) (*TestMessage, error) {
	return message, nil
}

func (s *TestService) TestFunc(_ *Session, message *TestMessage) (*TestMessage, error) {
	return message, nil
}

func (s *TestService) TestFunc1(_ *Session, _ *TestMessage) error {
	return nil
}

func (s *TestService) TestFunc2(_ *Session, _ *TestMessage) {
}

func (s *TestService) TestFunc3(_ *TestMessage, _ *TestMessage) error {
	return nil
}

func (s *TestService) testFunc4(_ *Session, _ *TestMessage) error {
	return nil
}

func (s *TestService) TestFunc5(_ *TestMessage) error {
	return nil
}

func funcExist(m *CliServer, server, name string) bool {
	if s, ok := m.serverList[server]; ok {
		if _, ok = s.handlers[name]; ok {
			return true
		}
	}
	return false
}

func TestNewManager(t *testing.T) {
	m, err := NewCliServer("json_codec")
	if err != nil {
		t.Fatal(err)
	}
	err = m.RegisterOne("", &TestService{}, true)
	if err != nil {
		t.Fatal(err)
	}
	for _, v := range funcRegisterTest {
		if funcExist(m, "testservice", v.h) != v.exist {
			t.Errorf("func %s, check failed.  want %t", v.h, v.exist)
		}
	}
}
