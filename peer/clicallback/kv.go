package clicallback

import "sync"

type kvExt struct {
	k    interface{}
	v    interface{}
	data map[interface{}]interface{}
	l    sync.RWMutex
}

func (e *kvExt) Get(key interface{}) interface{} {
	e.l.RLock()
	defer e.l.RUnlock()
	return e.data[key]
}

func (e *kvExt) Set(key, value interface{}) {
	e.l.Lock()
	defer e.l.Unlock()
	if e.data == nil {
		e.data = make(map[interface{}]interface{})
	}
	e.data[key] = value
}
