package hook

import "github.com/sirupsen/logrus"

var (
	logrusFieldMap = logrus.FieldMap{
		logrus.FieldKeyTime: "timestamp",
		logrus.FieldKeyMsg:  "message",
		logrus.FieldKeyFunc: "funcname",
		logrus.FieldKeyFile: "filename",
	}
)
