package hook

import (
	"fmt"
	"github.com/lestrrat/go-file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"runtime"
	"time"
)

func NewLfsHook(fileName string) (logrus.Hook, error) {
	writer, err := rotatelogs.New(
		fileName+"_%Y%m%d%H.log",
		// WithLinkName为最新的日志建立软连接，以方便随着找到当前日志文件
		//rotatelogs.WithLinkName(fileName),
		rotatelogs.WithRotationTime(time.Hour),
		//rotatelogs.WithMaxAge(time.Hour*24*30),
	)
	if err != nil {
		return nil, fmt.Errorf("config local file system for logger error: %v", err)
	}
	lfsHook := lfshook.NewHook(lfshook.WriterMap{
		logrus.DebugLevel: writer,
		logrus.InfoLevel:  writer,
		logrus.WarnLevel:  writer,
		logrus.ErrorLevel: writer,
		logrus.FatalLevel: writer,
		logrus.PanicLevel: writer,
	}, &logrus.JSONFormatter{
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			return f.Function, fmt.Sprintf("%s %d", f.File, f.Line)
		},
		FieldMap: logrusFieldMap,
	})

	return lfsHook, nil
}
