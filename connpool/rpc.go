package connpool

import (
	"bufio"
	"encoding/gob"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/rpc"
	"time"
)

var connected = "200 Connected to Go RPC"

type RPCPool struct {
	pool Pool
}

func (p *RPCPool) Call(method string, arg, reply interface{}) error {
	sess, err := p.pool.Get()
	if err != nil {
		return err
	}
	defer p.pool.Put(sess)
	return sess.(*rpc.Client).Call(method, arg, reply)
}

func NewHttpRPCPool(network, address, path string, ping func(c *rpc.Client) error) (*RPCPool, error) {
	factory := func() (interface{}, error) {
		if network == "" {
			return nil, fmt.Errorf("network is empty.")
		}
		if address == "" {
			return nil, fmt.Errorf("address is empty.")
		}
		conn, err := net.DialTimeout(network, address, time.Second*2)
		if err != nil {
			return nil, err
		}
		io.WriteString(conn, "CONNECT "+path+" HTTP/1.0\n\n")
		resp, err := http.ReadResponse(bufio.NewReader(conn), &http.Request{Method: "CONNECT"})
		if err == nil && resp.Status == connected {
			encBuf := bufio.NewWriter(conn)
			p := rpc.NewClientWithCodec(&Codec{
				Closer:  conn,
				Decoder: gob.NewDecoder(conn),
				Encoder: gob.NewEncoder(conn),
				EncBuf:  encBuf,
				Timeout: time.Second * 2,
			})
			return p, nil
		}
		if err == nil {
			err = fmt.Errorf("unexpected HTTP response: %s", resp.Status)
		}
		conn.Close()
		return nil, &net.OpError{
			Op:   "dial-http",
			Net:  network + " " + address,
			Addr: nil,
			Err:  err,
		}
	}
	close := func(c interface{}) error {
		return c.(*rpc.Client).Close()
	}
	var pingFunc func(interface{}) error
	if ping != nil {
		pingFunc = func(c interface{}) error {
			return ping(c.(*rpc.Client))
		}
	}
	pool, err := NewChannelPool(&Config{
		InitialCap:  5,
		MaxCap:      30,
		Factory:     factory,
		Close:       close,
		Ping:        pingFunc,
		IdleTimeout: time.Minute * 30,
	})
	if err != nil {
		return nil, err
	}
	return &RPCPool{
		pool: pool,
	}, nil
}

func NewRPCPool(network, address string, ping func(c *rpc.Client) error) (*RPCPool, error) {
	factory := func() (interface{}, error) {
		if network == "" {
			return nil, fmt.Errorf("network is empty.")
		}
		if address == "" {
			return nil, fmt.Errorf("address is empty.")
		}
		conn, err := net.DialTimeout(network, address, time.Second*2)
		if err != nil {
			return nil, err
		}
		encBuf := bufio.NewWriter(conn)
		p := rpc.NewClientWithCodec(&Codec{
			Closer:  conn,
			Decoder: gob.NewDecoder(conn),
			Encoder: gob.NewEncoder(conn),
			EncBuf:  encBuf,
			Timeout: time.Second * 2,
		})
		return p, nil
	}
	close := func(c interface{}) error {
		return c.(*rpc.Client).Close()
	}
	var pingFunc func(interface{}) error
	if ping != nil {
		pingFunc = func(c interface{}) error {
			return ping(c.(*rpc.Client))
		}
	}
	pool, err := NewChannelPool(&Config{
		InitialCap:  5,
		MaxCap:      30,
		Factory:     factory,
		Close:       close,
		Ping:        pingFunc,
		IdleTimeout: time.Minute * 30,
	})
	if err != nil {
		return nil, err
	}
	return &RPCPool{
		pool: pool,
	}, nil
}
