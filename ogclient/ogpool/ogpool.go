package ogpool

import (
	"fmt"
	"gitlab.stars.game/ogbg/tools/connpool"
	"net"
	"net/url"
	"time"
)

func NewOGDBPool(addr string, initCap, maxCap int, idleTimeOut time.Duration) (connpool.Pool, error) {
	u, err := url.Parse(addr)
	if err != nil {
		return nil, fmt.Errorf("invalid address. addr:%s err:%v", addr, err)
	}
	factory := func() (interface{}, error) {
		//conn, err := net.DialTimeout(u.Scheme, u.Host+u.Port(), time.Second*2)
		//if err != nil {
		//	return nil, err
		//}
		//encBuf := bufio.NewWriter(conn)
		//p := rpc.NewClientWithCodec(&Codec{
		//	Closer:  conn,
		//	Decoder: gob.NewDecoder(conn),
		//	Encoder: gob.NewEncoder(conn),
		//	EncBuf:  encBuf,
		//	Timeout: time.Second * 2,
		//})
		return net.DialTimeout(u.Scheme, u.Host, time.Second*2)
	}
	close := func(c interface{}) error {
		return c.(net.Conn).Close()
	}
	//var pingFunc func(interface{}) error
	//if ping != nil {
	//	pingFunc = func(c interface{}) error {
	//		return ping(c.(*rpc.Client))
	//	}
	//}
	pool, err := connpool.NewChannelPool(&connpool.Config{
		InitialCap:  initCap,
		MaxCap:      maxCap,
		Factory:     factory,
		Close:       close,
		Ping:        nil,
		IdleTimeout: idleTimeOut,
	})
	if err != nil {
		return nil, err
	}
	return pool, nil
}
