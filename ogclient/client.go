package ogclient

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"gitlab.stars.game/ogbg/tools/connpool"
	"gitlab.stars.game/ogbg/tools/ogclient/ogpool"
	"gitlab.stars.game/ogbg/tools/ogclient/protocol"
	"gitlab.stars.game/ogbg/tools/utils"
	"net"
	"time"
	"unsafe"
)

type OGClient struct {
	writeTimeOut time.Duration
	readTimeOut  time.Duration
	connPool     connpool.Pool
}

func NewOGClient(addr string, initCap, maxCap int, idleTimeOut, writeTimeOut, readTimeOut time.Duration) (*OGClient, error) {
	p, err := ogpool.NewOGDBPool(addr, initCap, maxCap, idleTimeOut)
	if err != nil {
		return nil, fmt.Errorf("create ogdb pool failed, addr:%s err:%v", addr, err)
	}
	return &OGClient{
		connPool:     p,
		writeTimeOut: writeTimeOut,
		readTimeOut:  readTimeOut,
	}, nil
}

func (oc *OGClient) GetScore(userName string, gameId int32, table int) (*protocol.GetScoreResponse, error) {
	conn, err := oc.connPool.Get()
	if err != nil {
		return nil, err
	}
	c := conn.(net.Conn)
	var bf bytes.Buffer
	var req protocol.GetScoreRequest
	req.GameId = gameId
	req.Table = int32(table)
	utils.StringToBytes(userName, req.UserName[0:])
	var reqHeader protocol.RequestHeader
	reqHeader.Msgid = protocol.GLID_GETSCORE
	reqHeader.Length = uint32(unsafe.Sizeof(req))
	binary.Write(&bf, binary.LittleEndian, &reqHeader)
	binary.Write(&bf, binary.LittleEndian, &req)
	if oc.writeTimeOut > 0 {
		c.SetWriteDeadline(time.Now().Add(oc.writeTimeOut))
	}
	if _, err = c.Write(bf.Bytes()); err != nil {
		oc.connPool.Close(conn)
		return nil, fmt.Errorf("write failed, err : %v", err)
	}
	var rd = make([]byte, 4096)
	for {
		if oc.readTimeOut > 0 {
			c.SetReadDeadline(time.Now().Add(oc.readTimeOut))
		}
		if _, err = c.Read(rd); err != nil {
			oc.connPool.Close(conn)
			return nil, fmt.Errorf("read failed, err : %v", err)
		} else {
			reader := bytes.NewBuffer(rd)
			var respHeader protocol.ResponseHeader
			binary.Read(reader, binary.LittleEndian, &respHeader)
			if (respHeader.Msgid & protocol.GLID_ACK) == protocol.GLID_ACK {
				realID := respHeader.Msgid - protocol.GLID_ACK
				switch realID {
				case protocol.GLID_GETSCORE:
					oc.connPool.Put(conn)
					var ack protocol.GetScoreResponse
					binary.Read(reader, binary.LittleEndian, &ack)
					return &ack, nil
				}
			}
		}
	}
}

func (oc *OGClient) PowerGameCoinChargeLong(userName string, gameCoin int64, gameId int32, tableNo, productType int, ip string) (uint32, *protocol.PowerGameCoinChargeLongResponse, error) {
	conn, err := oc.connPool.Get()
	if err != nil {
		return 0, nil, err
	}
	c := conn.(net.Conn)
	var bf bytes.Buffer
	var req protocol.PowerGameCoinChargeLongRequest
	req.GameId = gameId
	req.TableNo = int32(tableNo)
	req.ProductType = int32(productType)
	//req.Action = action
	req.GameCoin = gameCoin
	utils.StringToBytes(userName, req.UserName[0:])
	utils.StringToBytes(ip, req.UserIp[0:])
	var reqHeader protocol.RequestHeader
	reqHeader.Msgid = protocol.GLID_POWERGAMECOIN_CHANGE
	reqHeader.Length = uint32(unsafe.Sizeof(req))
	binary.Write(&bf, binary.LittleEndian, &reqHeader)
	binary.Write(&bf, binary.LittleEndian, &req)
	if oc.writeTimeOut > 0 {
		c.SetWriteDeadline(time.Now().Add(oc.writeTimeOut))
	}
	if _, err = c.Write(bf.Bytes()); err != nil {
		oc.connPool.Close(conn)
		return 0, nil, fmt.Errorf("write failed, err : %v", err)
	}
	var rd = make([]byte, 4096)
	for {
		if oc.readTimeOut > 0 {
			c.SetReadDeadline(time.Now().Add(oc.readTimeOut))
		}
		if _, err = c.Read(rd); err != nil {
			oc.connPool.Close(conn)
			return 0, nil, fmt.Errorf("read failed, err : %v", err)
		} else {
			reader := bytes.NewBuffer(rd)
			var respHeader protocol.ResponseHeader
			binary.Read(reader, binary.LittleEndian, &respHeader)
			if (respHeader.Msgid & protocol.GLID_ACK) == protocol.GLID_ACK {
				realID := respHeader.Msgid - protocol.GLID_ACK
				switch realID {
				case protocol.GLID_POWERGAMECOIN_CHANGE:
					oc.connPool.Put(conn)
					if respHeader.Result != OGSuccess {
						return 0, nil, fmt.Errorf("")
					}
					var ack protocol.PowerGameCoinChargeLongResponse
					binary.Read(reader, binary.LittleEndian, &ack)
					return respHeader.Result, &ack, nil
				}
			}
		}
	}
}

func (oc *OGClient) GetUserInfo(userName string, ticket string) (uint32, *protocol.GetUserInfoResponse, error) {
	conn, err := oc.connPool.Get()
	if err != nil {
		return 0, nil, err
	}
	c := conn.(net.Conn)
	var bf bytes.Buffer
	var req protocol.GetUserInfoRequest
	req.UserIp = 0x7f000001
	utils.StringToBytes(userName, req.UserName[0:])
	utils.StringToBytes(ticket, req.Ticket[0:])
	var reqHeader protocol.RequestHeader
	reqHeader.Msgid = protocol.GLID_GETUSERINFO2
	reqHeader.Length = uint32(unsafe.Sizeof(req))
	binary.Write(&bf, binary.LittleEndian, &reqHeader)
	binary.Write(&bf, binary.LittleEndian, &req)
	if oc.writeTimeOut > 0 {
		c.SetWriteDeadline(time.Now().Add(oc.writeTimeOut))
	}
	if _, err = c.Write(bf.Bytes()); err != nil {
		oc.connPool.Close(conn)
		return 0, nil, fmt.Errorf("write failed, err : %v", err)
	}
	var rd = make([]byte, 4096)
	for {
		if oc.readTimeOut > 0 {
			c.SetReadDeadline(time.Now().Add(oc.readTimeOut))
		}
		if _, err = c.Read(rd); err != nil {
			oc.connPool.Close(conn)
			return 0, nil, fmt.Errorf("read failed, err : %v", err)
		} else {
			reader := bytes.NewBuffer(rd)
			var respHeader protocol.ResponseHeader
			binary.Read(reader, binary.LittleEndian, &respHeader)
			if (respHeader.Msgid & protocol.GLID_ACK) == protocol.GLID_ACK {
				realID := respHeader.Msgid - protocol.GLID_ACK
				switch realID {
				case protocol.GLID_GETUSERINFO2:
					oc.connPool.Put(conn)
					var ack protocol.GetUserInfoResponse
					binary.Read(reader, binary.LittleEndian, &ack)
					return respHeader.Result, &ack, nil
				}
			}
		}
	}
}

func (oc *OGClient) GetUserDetailInfo(userName, password string) (*protocol.AuthUserDetailResponse, error) {
	conn, err := oc.connPool.Get()
	if err != nil {
		return nil, err
	}
	c := conn.(net.Conn)
	var bf bytes.Buffer
	var req protocol.AuthUserDetailRequest
	req.UserIp = 0x7f000001
	utils.StringToBytes(userName, req.UserName[0:])
	utils.StringToBytes(password, req.Password[0:])
	var reqHeader protocol.RequestHeader
	reqHeader.Msgid = protocol.GLID_GETUSERDETAILINFO2
	reqHeader.Length = uint32(unsafe.Sizeof(req))
	binary.Write(&bf, binary.LittleEndian, &reqHeader)
	binary.Write(&bf, binary.LittleEndian, &req)
	if oc.writeTimeOut > 0 {
		c.SetWriteDeadline(time.Now().Add(oc.writeTimeOut))
	}
	if _, err = c.Write(bf.Bytes()); err != nil {
		oc.connPool.Close(conn)
		return nil, fmt.Errorf("write failed, err : %v", err)
	}
	var rd = make([]byte, 4096)
	for {
		if oc.readTimeOut > 0 {
			c.SetReadDeadline(time.Now().Add(oc.readTimeOut))
		}
		if _, err = c.Read(rd); err != nil {
			oc.connPool.Close(conn)
			return nil, fmt.Errorf("read failed, err : %v", err)
		} else {
			reader := bytes.NewBuffer(rd)
			var respHeader protocol.ResponseHeader
			binary.Read(reader, binary.LittleEndian, &respHeader)
			if (respHeader.Msgid & protocol.GLID_ACK) == protocol.GLID_ACK {
				realID := respHeader.Msgid - protocol.GLID_ACK
				switch realID {
				case protocol.GLID_GETUSERDETAILINFO2:
					oc.connPool.Put(conn)
					var ack protocol.AuthUserDetailResponse
					binary.Read(reader, binary.LittleEndian, &ack)
					return &ack, nil
				}
			}
		}
	}
}

func (oc *OGClient) GetTempTicket(userName string, ticket string) (uint32, *protocol.GetUserTicketResponse, error) {
	conn, err := oc.connPool.Get()
	if err != nil {
		return 0, nil, err
	}
	c := conn.(net.Conn)
	var bf bytes.Buffer
	var req protocol.GetUserTicketRequest
	utils.StringToBytes(userName, req.UserName[0:])
	utils.StringToBytes(ticket, req.Ticket[0:])
	var reqHeader protocol.RequestHeader
	reqHeader.Msgid = protocol.GLID_GETTEMPTICKET
	reqHeader.Length = uint32(unsafe.Sizeof(req))
	binary.Write(&bf, binary.LittleEndian, &reqHeader)
	binary.Write(&bf, binary.LittleEndian, &req)
	if oc.writeTimeOut > 0 {
		c.SetWriteDeadline(time.Now().Add(oc.writeTimeOut))
	}
	if _, err = c.Write(bf.Bytes()); err != nil {
		oc.connPool.Close(conn)
		return 0, nil, fmt.Errorf("write failed, err : %v", err)
	}
	var rd = make([]byte, 4096)
	for {
		if oc.readTimeOut > 0 {
			c.SetReadDeadline(time.Now().Add(oc.readTimeOut))
		}
		if _, err = c.Read(rd); err != nil {
			oc.connPool.Close(conn)
			return 0, nil, fmt.Errorf("read failed, err : %v", err)
		} else {
			reader := bytes.NewBuffer(rd)
			var respHeader protocol.ResponseHeader
			binary.Read(reader, binary.LittleEndian, &respHeader)
			if (respHeader.Msgid & protocol.GLID_ACK) == protocol.GLID_ACK {
				realID := respHeader.Msgid - protocol.GLID_ACK
				switch realID {
				case protocol.GLID_GETTEMPTICKET:
					oc.connPool.Put(conn)
					var ack protocol.GetUserTicketResponse
					binary.Read(reader, binary.LittleEndian, &ack)
					return respHeader.Result, &ack, nil
				}
			}
		}
	}
}
