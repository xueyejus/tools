package fcmapi

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.stars.game/ogbg/tools/log"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

var logger *logrus.Entry
var _gameName, _gameId, _apiURL string

func Init(url, gameName, gameId string) error {
	logger = log.GetLogger().WithField("package", "fcmapi")
	_gameName = gameName
	_gameId = gameId
	_apiURL = url
	return nil
}

func FCMGameUp4Other(data []*FCMGameUp4OtherItem) (string, error) {
	if _apiURL == "" {
		return "", nil
	}
	client := http.Client{
		Timeout: time.Second * 2,
	}
	for _, v := range data {
		v.Gameid = _gameId
	}
	reqData := FCMGameUp4OtherReq{
		Game: _gameName,
		Data: data,
	}
	rb, err := json.Marshal(&reqData)
	if err != nil {
		return "", fmt.Errorf("json.Marshal failed. api:%s req:%v err:%v", _apiURL, reqData, err)
	}
	var values = url.Values{}
	values.Add("gameJson", string(rb))
	resp, err := client.PostForm(_apiURL, values)
	if err != nil {
		return "", fmt.Errorf("post failed. api:%s values:%s err:%v", _apiURL, values.Encode(), err)
	}
	defer resp.Body.Close()
	respStr, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("post read resp failed. api:%s values:%s err:%v", _apiURL, values.Encode(), err)
	}
	return string(respStr), nil
}
