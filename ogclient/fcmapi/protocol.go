package fcmapi

type FCMGameUp4OtherItem struct {
	Usesrname string `json:"username"`
	Gameid    string `json:"gameid"`
	Pottype   string `json:"pottype"`
	Time      string `json:"time"`
}

type FCMGameUp4OtherReq struct {
	Game string                 `json:"game"`
	Data []*FCMGameUp4OtherItem `json:"data"`
}
