package ogclient

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"gitlab.stars.game/ogbg/tools/connpool"
	"gitlab.stars.game/ogbg/tools/ogclient/ogpool"
	"gitlab.stars.game/ogbg/tools/ogclient/protocol"
	"gitlab.stars.game/ogbg/tools/utils"
	"net"
	"time"
	"unsafe"
)

type OGChatClient struct {
	connPool connpool.Pool
}

func NewOGChatClient(addr string, initCap, maxCap int, timeOut time.Duration) (*OGChatClient, error) {
	p, err := ogpool.NewOGDBPool(addr, initCap, maxCap, timeOut)
	if err != nil {
		return nil, fmt.Errorf("create og chat pool failed, addr:%s err:%v", addr, err)
	}
	return &OGChatClient{
		connPool: p,
	}, nil
}

func (oc *OGChatClient) BroadCast(templateID int, message string) error {
	conn, err := oc.connPool.Get()
	if err != nil {
		return err
	}
	c := conn.(net.Conn)
	var bf bytes.Buffer
	var req protocol.Chatsub2BroadcastMessageRequestBody
	req.TemplateID = int32(templateID)
	utils.StringToBytes(message, req.Message[0:])
	var reqHeader protocol.RequestHeader
	reqHeader.Msgid = protocol.GLID_BROADCAST
	reqHeader.Length = uint32(unsafe.Sizeof(req))
	binary.Write(&bf, binary.LittleEndian, &reqHeader)
	binary.Write(&bf, binary.LittleEndian, &req)
	c.SetWriteDeadline(time.Now().Add(time.Second * 1))
	if _, err := c.Write(bf.Bytes()); err != nil {
		oc.connPool.Close(conn)
		return fmt.Errorf("write failed, err:%v", err)
	}
	var rd = make([]byte, 4096)
	for {
		c.SetReadDeadline(time.Now().Add(time.Second * 1))
		if _, err := c.Read(rd); err != nil {
			oc.connPool.Close(conn)
			return fmt.Errorf("read failed, err : %v", err)
		} else {
			reader := bytes.NewBuffer(rd)
			var respHeader protocol.ResponseHeader
			binary.Read(reader, binary.LittleEndian, &respHeader)
			if (respHeader.Msgid & protocol.GLID_ACK) == protocol.GLID_ACK {
				realID := respHeader.Msgid - protocol.GLID_ACK
				switch realID {
				case protocol.GLID_BROADCAST:
					oc.connPool.Put(conn)
					return nil
				}
			}
		}
	}
	oc.connPool.Put(conn)
	return nil
}
