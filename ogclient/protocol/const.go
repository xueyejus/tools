package protocol

const (
	GLID_ACK                  = 0x80000000
	GLID_GETSCORE             = 0x864
	GLID_GETUSERINFO2         = 0x65D
	GLID_GETTEMPTICKET        = 0x65B
	GLID_GETUSERDETAILINFO2   = 0x40013
	GLID_POWERGAMECOIN_CHANGE = 0x4000C

	GLID_BROADCAST = 0x42E

	OGUserNameMaxLen = 20
	OGUserIPMaxLen   = 16
	OGErrorMaxLen    = 64
	OGPasswordMaxLen = 16
	OGTicketMaxLen   = 128
	OGNickNameMaxLen = 20
	OGFactionMaxLen  = 20
	OGDutyMaxLen     = 20
	OGMobileMaxLen   = 20
	OGEmailMaxLen    = 64

	OGStateMaxLen      = 20
	OGCityMaxLen       = 20
	OGAddressMaxLen    = 40
	OGPostalCodeMaxLen = 12

	OGRealNameMaxLen = 20
	OGPhoneMaxLen    = 32
	OGMD5MaxLen      = 33

	OGFavorMaxLen = 50
)

type RequestHeader struct {
	Msgid  uint32
	Length uint32
}

type ResponseHeader struct {
	Msgid  uint32
	Length uint32
	Result uint32
}
