package protocol

type GetUserTicketRequest struct {
	UserName [OGUserNameMaxLen]byte
	Ticket   [OGTicketMaxLen]byte
}

type GetUserTicketResponse struct {
	Type     int32
	Ticket   [OGTicketMaxLen]byte
	Password [OGPasswordMaxLen]byte
}

type GetScoreRequest struct {
	UserName [OGUserNameMaxLen]byte
	GameId   int32
	Table    int32
}

type GetScoreResponse struct {
	Score        int32
	Wins         int32
	Losses       int32
	Draws        int32
	Rank         int32
	GameCount    int32
	BreakCount   int32
	Time         int32
	InitRank     int32
	InitScore    int32
	GameCoin     int32
	InitGameCoin int32
	Deposit      int32
	GameCoinLong int64
	DepositLong  int64
}

type GameCoinChange2ResponseRequest struct {
	UserName    [OGUserNameMaxLen]byte //20
	GameId      int32
	CoinType    int32
	Blank1      int32
	GameCoin    int64
	UserIp      [OGUserIPMaxLen]byte //16
	TableNo     int32
	ProductType int32
	Action      int32
	Blank2      int32
	Deposit     int64
	RealGameId  int32
	Blank3      int32
}

type GameCoinChange2ResponseResponse struct {
	Blank         int32
	GameCoinTotal int64
	Error         [OGErrorMaxLen]byte //64
}

type PowerGameCoinChargeLongRequest struct {
	UserName    [OGUserNameMaxLen]byte //20
	GameId      int32
	GameCoin    int64
	UserIp      [OGUserIPMaxLen]byte //16
	TableNo     int32
	ProductType int32
	Action      int32
	Blank       int32
}

type PowerGameCoinChargeLongResponse struct {
	Blank         int32
	GameCoinTotal int64
	Error         [OGErrorMaxLen]byte //64
}

type AuthUserDetailRequest struct {
	// 联众用户名
	UserName [OGUserNameMaxLen]byte //20
	// password
	Password [OGPasswordMaxLen]byte //16
	// ticket，在没有password的情况下验证ticket
	Ticket [OGTicketMaxLen]byte //128
	// #define GLREQGETUSERDETAILINFO2_FLAG_GENTICKETLONG		0x00000001		//是否要生成长效TICKET
	// #define GLREQGETUSERDETAILINFO2_FLAG_NOTCHECKPASS		0x00000002		//不检测密码或TICKET,对于信任机器可以直接取用户信息
	Flags int32
	// 用户IP
	UserIp int32
}

type AuthUserDetailResponse struct {
	// 字节对齐补位
	Blank1 int32
	// 在数据文件中的记录顺序,-1表示该记录没有在数据文件中有记录
	Pos int32
	// 最后一次访问或修改的日期
	LastAccess int32
	// 第一次修改的日期
	FirstModify int32
	// 校验和
	CheckSum int32
	// 用户名
	Username [OGUserNameMaxLen]byte //20
	// 中文名字
	Nickname [OGNickNameMaxLen]byte //20
	// 密码
	Password [OGPasswordMaxLen]byte //16
	// 帮派
	Faction [OGFactionMaxLen]byte //20
	// 职务
	Duty [OGDutyMaxLen]byte //20
	// 还有多少天过期, 原dwFreeDays
	Freedays int16
	// 人物形象, 原dwFreeDays
	Image int16
	// 数据库内读到的Money字段
	Cash int32
	// 追加的Money值
	CashPlus int32
	// 存款
	Deposit     int32
	DepositPlus int32
	// 贷款
	Loan     int32
	LoanPlus int32
	// 锁定财富值
	LockMoney     int32
	LockMoneyPlus int32
	// 游戏竞猜所得
	GameBet     int32
	GameBetPlus int32
	// 财富游戏所得
	MoneyGame     int32
	MoneyGamePlus int32
	// 信用度
	Credit     int16
	CreditPlus int16
	// 门派ID
	FactionId int32
	// 职务ID
	DutyId int32
	// 手机号码
	Mobile    [OGMobileMaxLen]byte //20
	Moral     int32
	MoralPlus int32
	// bit0=0表示读出后未更新过,=1表示更新过
	LastLogin   int32
	LastLoginIP int32
	Jz          int32
	// 联众币数量
	Coin     int32
	CoinPlus int32
	// 标志字段
	Flags int32
	// 用户会员过期日期，户口之后wFreeDays是在每次读取时动态计算出来的
	Expire int32
	// 用户租用的过期日期,0表示没有
	ExpireRent int32
	// 用户名保护的过期日期,0表示没有
	ExpireProtect int32
	// 数字ID
	DigitalId int64
	// 出生日期(YYYYMMDD)
	Born int32
	// 地区编码
	AreaNo int32
	// 会员资历起始时间
	MemberStart int32
	// 联众币消费积分
	CoinScore     int32
	CoinScorePlus int32
	// 会员持续起始时间
	MemberContinue int32
	// 已奖励会员持续天数
	ContinuePresent int16
	// 会员等级,由timeMemberStart计算而来，用户晋升时的奖励
	MemberRank int16
	// 身份证上出生日期
	IdcBorn int32
	// 身份证号码校验码,20
	IdcCheckSum string //20
	// 用户证件验证状态
	Idcstate uint8
	// 防沉迷已提示用户次数,最高位标明是否已修改，只有修改过才更新到DB
	IdcTipCount uint8
	// 字节对齐补位
	Blank2 int16
	/**
	 * 用户附加信息记录,用在服务器外部
	 * struct tagOUTUSEREXTRAINFO
	 *
	 * 包含的成员变量的字节总长度为433，且它的有效字节对齐值为4，(433+3)%4=0,因此需要3字节补位，实际长度为436字节
	 */

	// 在数据文件中的记录顺序,-1表示该记录没有在数据文件中有记录
	Pos_extra int32
	// 最后一次访问或修改的日期
	LastAccess_extra int32
	// 第一次修改的日期
	FirstModify_extra int32
	// 校验和
	CheckSum_extra int32
	// 用户名
	Username_extra [OGUserNameMaxLen]byte //20
	// 修改标志
	UpdateFlags int32
	// email
	Email [OGEmailMaxLen]byte //64
	// 省
	State [OGStateMaxLen]byte //20
	// 市
	City [OGCityMaxLen]byte //20
	// 地址
	Address [OGAddressMaxLen]byte //40
	// 邮编
	PostalCode [OGPostalCodeMaxLen]byte //12
	// 真实姓名
	RealName [OGRealNameMaxLen]byte //20
	// 电话
	Phone [OGPhoneMaxLen]byte //32
	// md5字符串长度
	Pwd2 [OGMD5MaxLen]byte //33
	// 在哪里上网
	WherePlay [20]byte //20
	// 爱好
	Favor [OGFavorMaxLen]byte //50
	// 喜欢的游戏
	LoveGame [50]byte //50
	Idc      [20]byte //20
	// 字节对齐补位 ((729+12)+3)%4=0
	Blank3 [3]byte //3
	// 用户注册日期
	Register int32
	// 国家代码
	Country uint8
	// 教育程度代码
	Education uint8
	// 职业代码
	Occupation uint8
	// 职务代码
	WorkDuty uint8
	// 收入代码
	Income uint8
	// 何处知晓代码
	Comefrom uint8
	// 是否申请了密码保护
	ApplySign uint8
	// 证件类型
	IDType uint8
	/**
	 * 外部使用标志信息
	 * struct tagOUTPRIVILEGEINFO
	 *
	 * 结构体tagOUTPRIVILEGEINFO的有效字节对齐值为4，且它的有效字节对齐值为4，根据字节对齐原则，需要4字节补位，实际长度为148字节
	 */
	PrivilegeGlobal int32
	PrivilegeLocal  int32
	// 显示标志
	IcoFlags int16
	// 字节对齐补位 ((754+12)+2)%4=0
	Blank4 int16
	// 标志个数
	PrivilegeIcon int32
	// 缺省显示标志
	PrivilegeDefault int16
	// 标志列表
	Privilege [64]uint16 //2,64
	// 字节对齐补位 ((890+12)+2)%8=0
	Blank5 int16
	// ticket
	Ticket [OGTicketMaxLen]byte //128
	//用户状态,UI里没有
	Status int32
	//新等级
	NewMemberRank int32
	//会员成长值
	MemberGrowthValue int32
	//新的下一等级还需天数
	NewNextMemberRankNeedDays int32
}

type GetUserInfoRequest struct {
	// 联众用户名
	UserName [OGUserNameMaxLen]byte //20
	// 用户IP
	UserIp int32
	// 0为不读取特权值,1为读取特权值,2表示是否读取详细信息,也就是说即使用户处于保护现金状态,也能读到用户的财富值
	Flags int32
	// ticket，在没有password的情况下验证ticket
	Ticket [OGTicketMaxLen]byte
}

type GetUserInfoResponse struct {
	// 字节对齐补位
	//@FieldParam(index = 0, length = 4, fieldType = FieldType.STRING)
	//private String Blank1;
	Blank1 [4]byte

	// 在数据文件中的记录顺序,-1表示该记录没有在数据文件中有记录
	//@FieldParam(index = 4, length = 4, fieldType = FieldType.INT)
	//private int pos;
	Pos int32

	// 最后一次访问或修改的日期
	//@FieldParam(index = 8, length = 4, fieldType = FieldType.INT)
	//private int lastAccess;
	LastAccess int32

	// 第一次修改的日期
	//@FieldParam(index = 12, length = 4, fieldType = FieldType.INT)
	//private int firstModify;
	FirstModify int32

	// 校验和
	//@FieldParam(index = 16, length = 4, fieldType = FieldType.INT)
	//private int checkSum;
	CheckSum int32

	// 用户名
	//@FieldParam(index = 20, length = 20, fieldType = FieldType.STRING)
	//private String username;
	Username [OGUserNameMaxLen]byte

	// 中文名字
	//@FieldParam(index = 40, length = 20, fieldType = FieldType.STRING)
	//private String nickname;
	Nickname [OGNickNameMaxLen]byte

	// 密码
	//@FieldParam(index = 60, length = 16, fieldType = FieldType.STRING)
	//private String password;
	Password [OGPasswordMaxLen]byte

	// 帮派
	//@FieldParam(index = 76, length = 20, fieldType = FieldType.STRING)
	//private String faction;
	Faction [OGFactionMaxLen]byte

	// 职务
	//@FieldParam(index = 96, length = 20, fieldType = FieldType.STRING)
	//private String duty;
	Duty [OGDutyMaxLen]byte

	// 还有多少天过期, 原dwFreeDays
	//@FieldParam(index = 116, length = 2, fieldType = FieldType.SHORT)
	//private short freedays;
	FreeDays int16

	// 人物形象, 原dwFreeDays
	//@FieldParam(index = 118, length = 2, fieldType = FieldType.SHORT)
	//private short image;
	Image int16

	// 数据库内读到的Money字段
	//@FieldParam(index = 120, length = 4, fieldType = FieldType.INT)
	//private int cash;
	Cash int32

	// 追加的Money值
	//@FieldParam(index = 124, length = 4, fieldType = FieldType.INT)
	//private int cashPlus;
	CashPlus int32

	// 存款
	//@FieldParam(index = 128, length = 4, fieldType = FieldType.INT)
	//private int deposit;
	//@FieldParam(index = 132, length = 4, fieldType = FieldType.INT)
	//private int depositPlus;

	Deposit     int32
	DepositPlus int32

	// 贷款
	//@FieldParam(index = 136, length = 4, fieldType = FieldType.INT)
	//private int loan;
	//@FieldParam(index = 140, length = 4, fieldType = FieldType.INT)
	//private int loanPlus;
	Loan     int32
	LoanPlus int32

	// 锁定财富值
	//@FieldParam(index = 144, length = 4, fieldType = FieldType.INT)
	//private int lockMoney;
	//@FieldParam(index = 148, length = 4, fieldType = FieldType.INT)
	//private int lockMoneyPlus;
	LockMoney     int32
	LockMoneyPlus int32

	// 游戏竞猜所得
	//@FieldParam(index = 152, length = 4, fieldType = FieldType.INT)
	//private int gameBet;
	//@FieldParam(index = 156, length = 4, fieldType = FieldType.INT)
	//private int gameBetPlus;
	GameBet     int32
	GameBetPlus int32

	// 财富游戏所得
	//@FieldParam(index = 160, length = 4, fieldType = FieldType.INT)
	//private int moneyGame;
	//@FieldParam(index = 164, length = 4, fieldType = FieldType.INT)
	//private int moneyGamePlus;
	MoneyGame     int32
	MoneyGamePlus int32

	// 信用度
	//@FieldParam(index = 168, length = 2, fieldType = FieldType.SHORT)
	//private short credit;
	//@FieldParam(index = 170, length = 2, fieldType = FieldType.SHORT)
	//private short creditPlus;
	Credit     int16
	CreditPlus int16

	// 门派ID
	//@FieldParam(index = 172, length = 4, fieldType = FieldType.INT)
	//private int factionId;
	FactionId int32

	// 职务ID
	//@FieldParam(index = 176, length = 4, fieldType = FieldType.INT)
	//private int dutyId;
	DutyId int32

	// 手机号码
	//@FieldParam(index = 180, length = 20, fieldType = FieldType.STRING)
	//private String mobile;
	Mobile [OGMobileMaxLen]byte

	//@FieldParam(index = 200, length = 4, fieldType = FieldType.INT)
	//private int moral;
	Moral int32

	//@FieldParam(index = 204, length = 4, fieldType = FieldType.INT)
	//private int moralPlus;
	MoralPlus int32

	// bit0=0表示读出后未更新过,=1表示更新过
	//@FieldParam(index = 208, length = 4, fieldType = FieldType.INT)
	//private int lastLogin;
	LastLogin int32

	//@FieldParam(index = 212, length = 4, fieldType = FieldType.INT)
	//private int lastLoginIP;
	LastLoginIP int32

	//@FieldParam(index = 216, length = 4, fieldType = FieldType.INT)
	//private int jz;
	Jz int32

	// 联众币数量
	//@FieldParam(index = 220, length = 4, fieldType = FieldType.INT)
	//private int coin;
	//@FieldParam(index = 224, length = 4, fieldType = FieldType.INT)
	//private int coinPlus;
	Coin     int32
	CoinPlus int32

	// 标志字段
	//@FieldParam(index = 228, length = 4, fieldType = FieldType.INT)
	//private int flags;
	Flags int32

	// 用户会员过期日期，户口之后wFreeDays是在每次读取时动态计算出来的
	//@FieldParam(index = 232, length = 4, fieldType = FieldType.INT)
	//private int expire;
	Expire int32

	// 用户租用的过期日期,0表示没有
	//@FieldParam(index = 236, length = 4, fieldType = FieldType.INT)
	//private int expireRent;
	ExpireRent int32

	// 用户名保护的过期日期,0表示没有
	//@FieldParam(index = 240, length = 4, fieldType = FieldType.INT)
	//private int expireProtect;
	ExpireProtect int32

	// 数字ID
	//@FieldParam(index = 244, length = 8, fieldType = FieldType.LONG)
	//private long digitalId;
	DigitalId int64

	// 出生日期(YYYYMMDD)
	//@FieldParam(index = 252, length = 4, fieldType = FieldType.INT)
	//private int born;
	Born int32

	// 地区编码
	//@FieldParam(index = 256, length = 4, fieldType = FieldType.INT)
	//private int areaNo;
	AreaNo int32

	// 会员资历起始时间
	//@FieldParam(index = 260, length = 4, fieldType = FieldType.INT)
	//private int memberStart;
	MemberStart int32

	// 联众币消费积分
	//@FieldParam(index = 264, length = 4, fieldType = FieldType.INT)
	//private int coinScore;
	//@FieldParam(index = 268, length = 4, fieldType = FieldType.INT)
	//private int coinScorePlus;
	CoinScore     int32
	CoinScorePlus int32

	// 会员持续起始时间
	//@FieldParam(index = 272, length = 4, fieldType = FieldType.INT)
	//private int memberContinue;
	MemberContinue int32

	// 已奖励会员持续天数
	//@FieldParam(index = 276, length = 2, fieldType = FieldType.SHORT)
	//private short continuePresent;
	ContinuePresent int16

	// 会员等级,由timeMemberStart计算而来，用户晋升时的奖励
	//@FieldParam(index = 278, length = 2, fieldType = FieldType.SHORT)
	//private short memberRank;
	MemberRank int16

	// 身份证上出生日期
	//@FieldParam(index = 280, length = 4, fieldType = FieldType.INT)
	//private int idcBorn;
	IdcBorn int32

	// 身份证号码校验码,20
	//@FieldParam(index = 284, length = 20, fieldType = FieldType.STRING)
	//private String idcCheckSum;
	IdcCheckSum [20]byte

	// 用户证件验证状态
	//@FieldParam(index = 304, length = 1, fieldType = FieldType.BYTE)
	//private byte idcstate;
	Idcstate int8

	// 防沉迷已提示用户次数,最高位标明是否已修改，只有修改过才更新到DB
	//@FieldParam(index = 305, length = 1, fieldType = FieldType.BYTE)
	//private byte idcTipCount;
	IdcTipCount int8

	// 字节对齐补位
	//@FieldParam(index = 306, length = 2, fieldType = FieldType.STRING)
	//private String Blank2;
	Blank2 [2]byte

	//@FieldParam(index = 308, length = 4, fieldType = FieldType.INT)
	//private int privilege;
	Privilege int32

	// ticket
	//@FieldParam(index = 312, length = 128, fieldType = FieldType.STRING)
	//private String ticket;
	Ticket [OGTicketMaxLen]byte

	//@FieldParam(index = 440, length = 4, fieldType = FieldType.INT)
	//private int status;
	Status int32
}
