package ogclient

import (
	"fmt"
	"testing"
	"time"
)

var og *OGClient
var ogChat *OGChatClient

func init() {
	og, _ = NewOGClient("tcp://172.28.41.90:6000", 1, 1, time.Second*120, time.Second*10, time.Second*10)
	ogChat, _ = NewOGChatClient("tcp://172.28.41.90:2002", 1, 1, time.Second*120)
}

func TestOGClient_GetScore(t *testing.T) {
	resp, err := og.GetScore("春花秋月", OGGameIDGameCoin, 8)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(resp)
}

func TestOGClient_GetUserInfo(t *testing.T) {
	code, resp, err := og.GetUserInfo("jade22", "jade22")
	if err != nil {
		t.Fatal(err)
	}
	if code != OGSuccess {
		t.Fatal(code)
	}
	fmt.Println(string(resp.Ticket[0:]))
}

func TestOGClient_GetTempTicket(t *testing.T) {
	code, resp, err := og.GetTempTicket("jade22", "0173775506B0F03A165B541F00752F3B433D5D496B727755733F2D5AA2C17E3CF86544710C4E9DA87D378EF9DE7557A216FD562DF8869A907D2071BA6B7277")
	if err != nil {
		t.Fatal(err)
	}
	if code != OGSuccess {
		t.Fatal(code)
	}
	fmt.Println(string(resp.Ticket[0:]))
}

func TestOGClient_PowerGameCoinChargeLong(t *testing.T) {
	for i := 41; i < 60; i++ {
		user := fmt.Sprintf("jade%d", i)
		code, resp, err := og.PowerGameCoinChargeLong(user, 200000000, OGGameIDGameCoin, 8, 923141101, "127.0.0.1")
		if err != nil {
			fmt.Println(err)
			continue
		}
		if code == OGSuccess {
			fmt.Println(user, resp)
		} else {
			fmt.Println("failed. ", user, " code:", code)
		}
	}
	//code, resp, err := og.PowerGameCoinChargeLong("jade33", 1000000000, OGGameIDGameCoin, 8, 923141101, "127.0.0.1")
	//if err != nil {
	//	t.Fatal(err)
	//}
	//if code != OGSuccess {
	//	t.Fatal(code)
	//}
	//fmt.Println(resp)
}

func TestOGChatClient_BroadCast(t *testing.T) {
	err := ogChat.BroadCast(21, "xueyejus,《蔬菜消消乐》, ,1万,http://msj.qa.junyue.online/")
	if err != nil {
		t.Fatal(err)
	}
}

func TestNewOGChatClient(t *testing.T) {
	//bytes := []byte{''}
	//fmt.Print(bytes)
	//utf8.EncodeRune()
	//str := ""
	//fmt.Print([]byte(str))
	//var s = []byte("春花秋月123321")
	//fmt.Println(s)

	for i := 0; i < 10; i++ {
		//if t.BingoFlag() == false {
		//	tr := time.Millisecond * time.Duration(rand.RangeInt64(100, 700))
		tableID := i
		fmt.Println(&tableID)
		//time.AfterFunc(tr, func() {
		//	r.chTableMarkOpt <- TableMarkOpt{
		//		SeatID:  seatID,
		//		TableID: tableID,
		//		Num:     card,
		//	}
		//})
		//}
	}
}
