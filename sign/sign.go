package sign

import "sync"

var signList = make(map[string]Sign)
var signListMutex sync.RWMutex

type Sign interface {
	// Sign 生成签名 签名内容 私钥
	Sign([]byte, []byte) (string, error)
	// Verify 验签数据 签名内容 公钥 签名
	Verify([]byte, []byte, string) error
}

func RegisterSignObject(name string, value Sign) {
	if value == nil {
		panic("sign: can not register empty object")
	}
	signListMutex.Lock()
	defer signListMutex.Unlock()
	if _, dup := signList[name]; dup {
		panic("sign: dumplicate key " + name)
	}
	signList[name] = value
}

func GetSignObject(name string) Sign {
	if v, ok := signList[name]; ok {
		return v
	}
	return nil
}
