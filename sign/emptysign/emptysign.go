package emptysign

import "gitlab.stars.game/ogbg/tools/sign"

type emptySign struct{}

func (*emptySign) Sign(_ []byte, _ []byte) (string, error) {
	return "", nil
}
func (*emptySign) Verify(_ []byte, _ []byte, _ string) error {
	return nil
}
func init() {
	sign.RegisterSignObject("empty_sign", &emptySign{})
}
