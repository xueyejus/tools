package gamebase

import "sync"

type ServerBase struct {
	// 玩家列表
	playerList      map[string]IPlayer
	playerListMutex sync.RWMutex
}
