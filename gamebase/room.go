package gamebase

import "time"

type RoomBase struct {
	roomID     uint64    //房间ID
	createTime time.Time //创建时间
}
