package gamebase

type Server interface {
	// OnInitGameData 初始化游戏数据
	OnInitGameData() error
	// OnRecvFromClient 监听客户端数据
	OnRecvFromClient() error

	OnJoinRoom() error

	OnCreateRoom() error

	OnLeaveRoom() error

	OnRemovePlayer() error

	OnDestroyRoom() error

	OnChangeRoom() error

	OnChangeCustomPlayerStatus() error

	OnChangePlayerNetworkState() error

	OnStartFrameSync() error

	OnStopFrameSync() error
}
