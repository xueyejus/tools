package gamebase

import (
	"gitlab.stars.game/ogbg/tools/peer/clicallback"
	"sync"
	"time"
)

type IPlayer interface {
	UserID() string
	// Send 发送消息
	Send(id string, msg interface{}) error
	// Client 获取初始链接
	Client() *clicallback.Session
	// Bind 绑定初始链接
	Bind(*clicallback.Session)
	// Close 关闭链接
	Close()
}

type BaseUserInfo struct {
	ID          uint64
	Userid      string
	DisplayName string
	Nickname    string
	ImgURL      string
	Channel     string //渠道号
}

type BasePlayer struct {
	client *clicallback.Session

	id          uint64
	userid      string
	displayName string
	nickname    string
	imgURL      string
	channel     string //渠道号

	loginTime time.Time
}

func NewBasePlayer(sess *clicallback.Session, u *BaseUserInfo) BasePlayer {
	return BasePlayer{
		client:      sess,
		id:          u.ID,
		userid:      u.Userid,
		displayName: u.DisplayName,
		nickname:    u.Nickname,
		imgURL:      u.ImgURL,
		channel:     u.Channel,
		loginTime:   time.Now(),
	}
}

func (p *BasePlayer) GameTime() time.Duration {
	if p == nil {
		return 0
	}
	return time.Now().Sub(p.loginTime)
}

func (p *BasePlayer) Client() *clicallback.Session {
	if p == nil {
		return nil
	}
	return p.client
}

func (p *BasePlayer) Send(id string, msg interface{}) error {
	if p == nil || p.client == nil {
		return nil
	}
	return p.client.Send(id, msg)
}

func (p *BasePlayer) Bind(c *clicallback.Session) {
	if p == nil {
		return
	}
	if p.client != nil && p.client != c {
		p.client.Close()
	}
	p.client = c
}

func (p *BasePlayer) Close() {
	if p == nil || p.client == nil {
		return
	}
	p.client.Close()
}

func (p *BasePlayer) PlayerID() uint64 {
	if p == nil {
		return 0
	}
	return p.id
}

func (p *BasePlayer) UserID() string {
	if p == nil {
		return ""
	}
	return p.userid
}

func (p *BasePlayer) NickName() string {
	if p == nil {
		return ""
	}
	return p.nickname
}

func (p *BasePlayer) DisplayName() string {
	if p == nil {
		return ""
	}
	return p.displayName
}

func (p *BasePlayer) ImgURL() string {
	if p == nil {
		return ""
	}
	return p.imgURL
}

func (p *BasePlayer) Channel() string {
	if p == nil {
		return ""
	}
	return p.channel
}

type PlayerManager struct {
	playerList      map[interface{}]IPlayer
	playerListMutex sync.RWMutex
}

func NewPlayerManager() *PlayerManager {
	return &PlayerManager{
		playerList: make(map[interface{}]IPlayer),
	}
}

// Range calls f sequentially for each key and value present in the map.
// If f returns false, range stops the iteration.
func (m *PlayerManager) Range(f func(interface{}, IPlayer) bool) {
	m.playerListMutex.RLock()
	defer m.playerListMutex.RUnlock()
	for k, v := range m.playerList {
		if f(k, v) == false {
			return
		}
	}
}

func (m *PlayerManager) BroadCast(id string, msg interface{}) {
	m.Range(func(_ interface{}, p IPlayer) bool {
		_ = p.Send(id, msg)
		return true
	})
}

// AddPlayer 加入玩家，返回当前数量
func (m *PlayerManager) AddPlayer(id interface{}, player IPlayer) int {
	m.playerListMutex.Lock()
	defer m.playerListMutex.Unlock()
	// 加入列表
	m.playerList[id] = player
	return len(m.playerList)
}

func (m *PlayerManager) GetPlayer(id interface{}) (IPlayer, bool) {
	m.playerListMutex.Lock()
	defer m.playerListMutex.Unlock()
	if v, ok := m.playerList[id]; ok {
		return v, ok
	}
	return nil, false
}

func (m *PlayerManager) RemovePlayer(id interface{}) bool {
	m.playerListMutex.Lock()
	defer m.playerListMutex.Unlock()
	if _, ok := m.playerList[id]; ok {
		delete(m.playerList, id)
		return true
	}
	return false
}

func (m *PlayerManager) Count() int {
	m.playerListMutex.RLock()
	defer m.playerListMutex.RUnlock()
	return len(m.playerList)
}
